import cv2
import numpy as np
import pyautogui
from time import sleep
import logging

logging.warn('start, res %s', pyautogui.size())
num = 0
while True:
    logging.warn('taking screenshot')

    image = pyautogui.screenshot()
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    image = cv2.resize(image, pyautogui.size())

    # cv2.imwrite('scr'+str(num)+'.jpg', image)
    num = num+1;

    # pyautogui.click(100, 100)
    # pyautogui.moveRel(0, 100)

    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    template = cv2.imread('skip_add.jpg', 0)
    w, h = template.shape[::-1]
    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where(res >= threshold)
    for pt in zip(*loc[::-1]):
        logging.warn('fount at %s %s, ', pt[0], pt[1])
        pyautogui.click(pt[0] + 10, pt[1] + 10)

    sleep(1)

# cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)
    #
